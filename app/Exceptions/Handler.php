<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($this->isBadRequestException($e)) {
            return new JsonResponse(['success' => false, 'message' => 'Bad request', 'errorCode' => 400], 400);
        }

        if ($this->isMethodNotAllowedException($e)) {
            return new JsonResponse(['success' => false, 'message' => 'Method not allowed', 'errorCode' => 405], 405);
        }

        if ($request->isJson()) {
            return new JsonResponse(['message' => $e->getMessage()], 500);
        }
        
        return parent::render($request, $e);
    }

    private function isMethodNotAllowedException(Exception $e)
    {
        return $e instanceof MethodNotAllowedHttpException;
    }

    private function isBadRequestException(Exception $e)
    {
        return $e instanceof BadRequestHttpException;
    }
}
