<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/api', function () {
    return view('api-list');
});
Route::get('docs', function(){
    return View::make('docs.api.index');
})->name('docs');

Route::group(['prefix' => '/v1'], function () {
    Route::get('/employee', 'ApiController@getEmployeesList')->name('employeeList.get');
    Route::get('/employee/{id}', 'ApiController@getEmployee')->name('employee.get');
    Route::put('/employee/{id}', 'ApiController@putEmployee')->name('employee.put');
    Route::post('/employee', 'ApiController@createEmployee')->name('employee.post');

    Route::get('/reference', 'ApiController@getReference')->name('reference.get');
    Route::get('/roles/me', 'ApiController@getMyRoles')->name('my-roles.get');

    Route::get('/role', 'ApiController@getRoles')->name('role.get');

    Route::get('/group', 'ApiController@getGroupsList')->name('groups.get');
    Route::get('/group/{id}', 'ApiController@getGroup')->name('group.get');
    Route::post('/group/', 'ApiController@postGroup')->name('group.post');
    Route::put('/group/{id}', 'ApiController@putGroup')->name('group.put');

    Route::any('/user/me', 'AuthController@getUserMe')->name('user-me.any');

    Route::get('/error/validation', 'ErrorsController@getValidationError')->name('validation-errors.get');
});

