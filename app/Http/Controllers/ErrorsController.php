<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Symfony\Component\HttpFoundation\JsonResponse;

class ErrorsController extends Controller
{
    /**
     * GET /api/error/validation
     *
     * @return JsonResponse
     */
    public function getValidationError()
    {
        $data = [
            "fieldErrors" => [
                [
                    "field" => "mobile",
                    "errorCode"=> 1,
                    "message" => "Mobile phone number is invalid",
                ],
                [
                    "field" => "email",
                    "errorCode"=> 1003,
                    "message" => "Email is invalid",
                ],
            ],
            "globalErrors" => [ ]
        ];

        return new JsonResponse($data, 409);
    }
}
