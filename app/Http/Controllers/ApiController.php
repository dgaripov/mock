<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class ApiController
 * @package App\Http\Controllers
 */
class ApiController extends Controller
{
    private $faker;

    public function __construct()
    {
        $this->faker = \Faker\Factory::create();
    }
    /**
     * GET /api/employee
     *
     * @return JsonResponse
     */
    public function getEmployeesList()
    {
        $employees = [
            $this->getOneEmployee(),
            $this->getOneEmployee(),
            $this->getOneEmployee(),
        ];

        if (request()->has('groupId')) {
            $employees = $this->filterEmployees($employees, ['groupId' => request('groupId')]);
        }

        sort($employees);

        $data = [
            'entities' => $employees,
            "page" => 0,
            "pageSize" => 0,
            "totalPageCount" => 1,
        ];

        return new JsonResponse($data);
    }

    /**
     * GET /api/employee/{id}
     *
     * @param string $id
     *
     * @return JsonResponse
     */
    public function getEmployee($id)
    {
        $data = $this->getOneEmployee($id);

        return new JsonResponse($data);
    }

    /**
     * PUT /api/employee/{id}
     *
     * @param string $id
     *
     * @return JsonResponse
     */
    public function putEmployee($id)
    {
        dsds();
        return new Response('', 204);
    }

    /**
     * POST /api/employee
     *
     * @return JsonResponse
     */
    public function createEmployee()
    {
        $content = request()->toArray();

        if (is_null($content)) {
            throw new BadRequestHttpException();
        }
        
        if (!isset($content['id'])) {
            $content['id'] = mt_rand(5000, 10000);
        }

        return new JsonResponse($content);
    }

    /**
     * GET /api/reference
     * 
     * @return JsonResponse
     */
    public function getReference()
    {
        $data = [
            [
                'name' => 'salutation',
                'values' => [
                    ['id' => $this->faker->uuid, 'value' => 'employee.personalDetails.salutation.mr'],
                    ['id' => $this->faker->uuid, 'value' => 'employee.personalDetails.salutation.mrs'],
                ],
            ],
            [
                'name' => 'contract_type',
                'values' => [
                    ['id' => $this->faker->uuid, 'value' => 'employee.jobDetails.contractType.fixedTerm'],
                    ['id' => $this->faker->uuid, 'value' => 'employee.jobDetails.contractType.freelancer'],
                ],
            ],
        ];

        return new JsonResponse($data);
    }

    /**
     * GET /api/roles/me
     *
     * @return JsonResponse
     */
    public function getMyRoles()
    {
        $data = User::myRolesMock();

        return new JsonResponse($data);
    }

    /**
     * GET /api/role
     *
     * @return JsonResponse
     */
    public function getRoles()
    {
        $data = [
            'total' => 2,
            'result' => [
                [
                    'id' => $this->faker->uuid,
                    'name' => 'Employee.delete',
                    'groups' => [
                        ['id' => $this->faker->uuid, 'name' => $this->faker->word],
                    ],
                ],
                [
                    'id' => 2,
                    'name' => 'Employee.update',
                    'groups' => [
                        ['id' => $this->faker->uuid, 'name' => $this->faker->word],
                    ],
                ],
            ],
        ];

        return new JsonResponse($data);
    }

    /**
     * GET /api/group/{id}
     *
     * @param $id
     *
     * @return JsonResponse
     */
    public function getGroup($id)
    {
        $data = [
            'id' => $id,
            'name' => ucfirst($this->faker->word),
            'roles' => [
                ['id' => $this->faker->uuid, 'name' => 'Employee.update'],
                ['id' => $this->faker->uuid, 'name' => 'Employee.delete'],
            ],
            'status' => $this->faker->numberBetween(0, 1),
        ];

        return new JsonResponse($data);
    }

    /**
     * GET /api/group
     *
     * @return JsonResponse
     */
    public function getGroupsList()
    {
        $groups = [
            [
                'id' => $this->faker->uuid,
                'name' => ucfirst($this->faker->word),
                'status' => $this->faker->numberBetween(0, 1),
                'created_by' => [
                    'id' => $this->faker->uuid,
                    'name' => 'Auto1 Admin'
                ]
            ],
            [
                'id' => $this->faker->uuid,
                'name' => ucfirst($this->faker->word),
                'status' => $this->faker->numberBetween(0, 1),
                'created_by' => [
                    'id' => $this->faker->uuid,
                    'name'=> 'Auto1 Admin'
                ]
            ]
        ];

        $data = [
            'entities' => $groups,
            "page" => 0,
            "pageSize" => 0,
            "totalPageCount" => 1,
        ];

        return new JsonResponse($data);
    }

    /**
     * POST /api/group
     *
     * @return JsonResponse
     */
    public function postGroup()
    {
        return $this->getGroup(1);
    }

    /**
     * PUT /api/group/{id}
     *
     * @param $id
     *
     * @return JsonResponse
     */
    public function putGroup($id)
    {
        return $this->getGroup($id);
    }

    private function getOneEmployee($id = null)
    {
        $faker = \Faker\Factory::create();

        return [
            "accessCountries" => [
                $faker->unique()->country,
                $faker->unique()->country,
                $faker->unique()->country,
            ],
            "bonusGross" =>  [
                "amount" =>  450000,
                "amountFormatted" =>  "4500,00",
                "currency" =>  "EUR"
            ],
            "contractType" =>  1,
            "country" =>  $faker->country,
            "department" =>  [
                "id" =>  $faker->unique()->uuid,
                "name" =>  $faker->bs,
                "status" =>  $faker->boolean(50),
            ],
            "email" =>  $faker->unique()->email,
            "employmentType" =>  $faker->numberBetween(1, 5),
            "endDate" =>  $faker->date('Y-m-d\TH:i:s\.uO'),
            "firstName" =>  $faker->firstName,
            "id" =>  is_null($id) ? $faker->unique()->uuid : $id,
            "lastName" =>  $faker->lastName,
            "middleName" =>  $faker->firstName,
            "personnelNumber" =>  $faker->numberBetween(10000, 99999),
            "position" =>  [
                "id" =>  $faker->unique()->uuid,
                "name" =>  $faker->jobTitle,
            ],
            "probationPeriod" =>  $faker->numberBetween(100, 300),
            "requestType" =>  $faker->boolean(50),
            "salaryGross" =>  [
                "amount" =>  5000000,
                "amountFormatted" =>  "5000,00",
                "currency" =>  "EUR"
            ],
            "salutation" =>  value(function() {
                $sals = ['mr', 'ms', 'mrs'];

                return $sals[rand(0, count($sals) - 1)];
            }),
            "startDate" =>  $faker->date('Y-m-d\TH:i:s\.uO'),
            "status" =>  $faker->boolean(50),
            "supervisor" =>  $faker->unique()->uuid,
            "userGroups" =>  [
                [
                    "createdBy" =>  $faker->unique()->uuid,
                    "id" =>  $faker->unique()->uuid,
                    "name" =>  "Moderator",
                    "roles" =>  [
                        [
                            "id" =>  $faker->unique()->uuid,
                            "name" =>  "Employee.create",
                        ],
                        [
                            "id" =>  $faker->unique()->uuid,
                            "name" =>  "Employee.edit",
                        ],
                    ],
                    "status" =>  $faker->numberBetween(0, 1),
                ]
            ],
            "workPlace" => $faker->company,
        ];
    }

    private function filterEmployees($employees, $filter)
    {
        $result = [];

        foreach ($employees as $employee) {
            foreach ($employee['group'] as $group) {
                if ($group['id'] == $filter['groupId']) {
                    $result[$employee['id']] = $employee;
                }
            }
        }

        return $result;
    }
}
