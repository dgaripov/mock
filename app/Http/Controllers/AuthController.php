<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Request;

/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{
    /**
     * GET /api/user/me
     *
     * @return JsonResponse
     */
    public function getUserMe(Request $request)
    {
        $faker = \Faker\Factory::create();

        $data = [
            'id' => $faker->uuid,
            'email' => $faker->email,
            'groups' => User::myGroupsMock(),
        ];
        $code = 200;

        $auth = Request::header('Authorization', false);

        if ($auth === false) {
            $data = [
                'message' => 'Unauthorized',
            ];
            $code = 401;
        }
        $token = explode(' ', $auth);

        if (isset($token[1]) && $token[1] == '1') {
            $data = [
                'message' => 'Forbidden',
            ];
            $code = 403;
        }

        return new JsonResponse($data, $code);
    }
}
