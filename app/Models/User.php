<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function myRolesMock()
    {
        return [
            'Employee.personalDetails.select',
            'Employee.personalDetails.update',
            'Employee.delete',
            'Department.*',
        ];
    }

    public static function myGroupsMock()
    {
        $faker = \Faker\Factory::create();
        return [
            [
                "createdBy" => $faker->unique()->uuid,
                "id" => $faker->unique()->uuid,
                "name" => ucfirst(implode(' ', $faker->unique()->words(2))),
                "roles" => [
                    [
                        "id" => $faker->unique()->uuid,
                        "name" => "Employee.create"
                    ],
                    [
                        "id" => $faker->unique()->uuid,
                        "name" => "Employee.edit"
                    ],
                ],
                "status" => (string) $faker->numberBetween(1, 0),
            ]
        ];
    }
}
