<!DOCTYPE html>
<html>
    <head>
        <title>Eagle Mock Web-Service List</title>

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container">
            <div class="well well-lg">
                <h1>
                    Api Documentation
                </h1>
                <br />
                <a href="{{ route('docs', ['#Api']) }}" class="btn btn-primary btn-lg btn-block">Api Docs</a>
            </div>
        </div>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    </body>
</html>
